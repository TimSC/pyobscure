#from PIL import Image, ImageDraw, ImageFont
import numpy as np
#from skimage import data, io, filters, color
#import colour
import random
from matplotlib import pyplot as plt

def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))

if __name__=="__main__":

	rc = np.random.random() * 255
	gc = np.random.random() * 255
	bc = np.random.random() * 255

	print (rc, gc, bc)

	scale = 1e3
	bscale = 1e3

	#Foreground and background probability distribution
	dist = np.zeros((256,256,256), dtype=np.double)
	col = np.zeros((256,256,256,3), dtype=np.uint8)

	for i in range(dist.shape[0]):
		for j in range(dist.shape[1]):
			dist[i, j, :] = np.linalg.norm([i-rc, j-gc, np.arange(dist.shape[2])-bc])
			col[i, j, :, 0] = i
			col[i, j, :, 1] = j
			col[i, j, :, 2] = np.arange(dist.shape[2])

	dist = dist.reshape((-1,))
	col = col.reshape((-1, 3))

	fg = gaussian(dist, 0.0, scale)
	bg = 1.0000001 - gaussian(dist, 0.0, bscale) #Slightly higher than 1 for numerical reasons

	#Normalize
	fg = fg / np.sum(fg)
	bg = bg / np.sum(bg) #This can get unstable if bg is almost zero everywhere

	im = np.zeros((640, 480, 3), dtype=np.uint8)
	
	im[:, :240, :] = col[np.random.choice(np.arange(fg.shape[0]), (640, 240), p=fg)]
	im[:, 240:, :] = col[np.random.choice(np.arange(bg.shape[0]), (640, 240), p=bg)]

	plt.imshow(im)
	plt.show()

